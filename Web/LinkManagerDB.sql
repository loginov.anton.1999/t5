PGDMP             	            x            LinkManagerDB    11.5    11.5 =    b           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            c           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            d           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            e           1262    22314    LinkManagerDB    DATABASE     �   CREATE DATABASE "LinkManagerDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE "LinkManagerDB";
             postgres    false            �            1259    22343    AspNetRoleClaims    TABLE     �   CREATE TABLE public."AspNetRoleClaims" (
    "Id" integer NOT NULL,
    "RoleId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text
);
 &   DROP TABLE public."AspNetRoleClaims";
       public         postgres    false            �            1259    22341    AspNetRoleClaims_Id_seq    SEQUENCE     �   ALTER TABLE public."AspNetRoleClaims" ALTER COLUMN "Id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."AspNetRoleClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    201            �            1259    22315    AspNetRoles    TABLE     �   CREATE TABLE public."AspNetRoles" (
    "Id" text NOT NULL,
    "Name" character varying(256),
    "NormalizedName" character varying(256),
    "ConcurrencyStamp" text
);
 !   DROP TABLE public."AspNetRoles";
       public         postgres    false            �            1259    22358    AspNetUserClaims    TABLE     �   CREATE TABLE public."AspNetUserClaims" (
    "Id" integer NOT NULL,
    "UserId" text NOT NULL,
    "ClaimType" text,
    "ClaimValue" text
);
 &   DROP TABLE public."AspNetUserClaims";
       public         postgres    false            �            1259    22356    AspNetUserClaims_Id_seq    SEQUENCE     �   ALTER TABLE public."AspNetUserClaims" ALTER COLUMN "Id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."AspNetUserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    203            �            1259    22371    AspNetUserLogins    TABLE     �   CREATE TABLE public."AspNetUserLogins" (
    "LoginProvider" text NOT NULL,
    "ProviderKey" text NOT NULL,
    "ProviderDisplayName" text,
    "UserId" text NOT NULL
);
 &   DROP TABLE public."AspNetUserLogins";
       public         postgres    false            �            1259    22384    AspNetUserRoles    TABLE     b   CREATE TABLE public."AspNetUserRoles" (
    "UserId" text NOT NULL,
    "RoleId" text NOT NULL
);
 %   DROP TABLE public."AspNetUserRoles";
       public         postgres    false            �            1259    22402    AspNetUserTokens    TABLE     �   CREATE TABLE public."AspNetUserTokens" (
    "UserId" text NOT NULL,
    "LoginProvider" text NOT NULL,
    "Name" text NOT NULL,
    "Value" text
);
 &   DROP TABLE public."AspNetUserTokens";
       public         postgres    false            �            1259    22323    AspNetUsers    TABLE     �  CREATE TABLE public."AspNetUsers" (
    "Id" text NOT NULL,
    "UserName" character varying(256),
    "NormalizedUserName" character varying(256),
    "Email" character varying(256),
    "NormalizedEmail" character varying(256),
    "EmailConfirmed" boolean NOT NULL,
    "PasswordHash" text,
    "SecurityStamp" text,
    "ConcurrencyStamp" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" boolean NOT NULL,
    "TwoFactorEnabled" boolean NOT NULL,
    "LockoutEnd" timestamp with time zone,
    "LockoutEnabled" boolean NOT NULL,
    "AccessFailedCount" integer NOT NULL,
    "DateRegister" timestamp without time zone NOT NULL
);
 !   DROP TABLE public."AspNetUsers";
       public         postgres    false            �            1259    22333    BadLinks    TABLE     W   CREATE TABLE public."BadLinks" (
    "Id" integer NOT NULL,
    "Url" text NOT NULL
);
    DROP TABLE public."BadLinks";
       public         postgres    false            �            1259    22331    BadLinks_Id_seq    SEQUENCE     �   ALTER TABLE public."BadLinks" ALTER COLUMN "Id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."BadLinks_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    199            �            1259    22432    Links    TABLE     #  CREATE TABLE public."Links" (
    "Id" integer NOT NULL,
    "Name" text NOT NULL,
    "Description" text,
    "DateCreate" timestamp without time zone NOT NULL,
    "IsActive" boolean NOT NULL,
    "ProjectId" integer NOT NULL,
    "Url" text NOT NULL,
    "CountClick" integer NOT NULL
);
    DROP TABLE public."Links";
       public         postgres    false            �            1259    22430    Links_Id_seq    SEQUENCE     �   ALTER TABLE public."Links" ALTER COLUMN "Id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."Links_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    210            �            1259    22417    Projects    TABLE     �   CREATE TABLE public."Projects" (
    "Id" integer NOT NULL,
    "Name" text NOT NULL,
    "Description" text,
    "DateCreate" timestamp without time zone NOT NULL,
    "UserId" text,
    "CountLink" integer NOT NULL
);
    DROP TABLE public."Projects";
       public         postgres    false            �            1259    22415    Projects_Id_seq    SEQUENCE     �   ALTER TABLE public."Projects" ALTER COLUMN "Id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."Projects_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    208            V          0    22343    AspNetRoleClaims 
   TABLE DATA               W   COPY public."AspNetRoleClaims" ("Id", "RoleId", "ClaimType", "ClaimValue") FROM stdin;
    public       postgres    false    201   �L       Q          0    22315    AspNetRoles 
   TABLE DATA               [   COPY public."AspNetRoles" ("Id", "Name", "NormalizedName", "ConcurrencyStamp") FROM stdin;
    public       postgres    false    196   �L       X          0    22358    AspNetUserClaims 
   TABLE DATA               W   COPY public."AspNetUserClaims" ("Id", "UserId", "ClaimType", "ClaimValue") FROM stdin;
    public       postgres    false    203   �L       Y          0    22371    AspNetUserLogins 
   TABLE DATA               m   COPY public."AspNetUserLogins" ("LoginProvider", "ProviderKey", "ProviderDisplayName", "UserId") FROM stdin;
    public       postgres    false    204   xM       Z          0    22384    AspNetUserRoles 
   TABLE DATA               ?   COPY public."AspNetUserRoles" ("UserId", "RoleId") FROM stdin;
    public       postgres    false    205   �M       [          0    22402    AspNetUserTokens 
   TABLE DATA               X   COPY public."AspNetUserTokens" ("UserId", "LoginProvider", "Name", "Value") FROM stdin;
    public       postgres    false    206   �M       R          0    22323    AspNetUsers 
   TABLE DATA               2  COPY public."AspNetUsers" ("Id", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount", "DateRegister") FROM stdin;
    public       postgres    false    197   �M       T          0    22333    BadLinks 
   TABLE DATA               1   COPY public."BadLinks" ("Id", "Url") FROM stdin;
    public       postgres    false    199   ]O       _          0    22432    Links 
   TABLE DATA               z   COPY public."Links" ("Id", "Name", "Description", "DateCreate", "IsActive", "ProjectId", "Url", "CountClick") FROM stdin;
    public       postgres    false    210   zO       ]          0    22417    Projects 
   TABLE DATA               f   COPY public."Projects" ("Id", "Name", "Description", "DateCreate", "UserId", "CountLink") FROM stdin;
    public       postgres    false    208   �Q       f           0    0    AspNetRoleClaims_Id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."AspNetRoleClaims_Id_seq"', 1, false);
            public       postgres    false    200            g           0    0    AspNetUserClaims_Id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."AspNetUserClaims_Id_seq"', 2, true);
            public       postgres    false    202            h           0    0    BadLinks_Id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public."BadLinks_Id_seq"', 1, false);
            public       postgres    false    198            i           0    0    Links_Id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public."Links_Id_seq"', 7, true);
            public       postgres    false    209            j           0    0    Projects_Id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public."Projects_Id_seq"', 4, true);
            public       postgres    false    207            �
           2606    22350 $   AspNetRoleClaims PK_AspNetRoleClaims 
   CONSTRAINT     h   ALTER TABLE ONLY public."AspNetRoleClaims"
    ADD CONSTRAINT "PK_AspNetRoleClaims" PRIMARY KEY ("Id");
 R   ALTER TABLE ONLY public."AspNetRoleClaims" DROP CONSTRAINT "PK_AspNetRoleClaims";
       public         postgres    false    201            �
           2606    22322    AspNetRoles PK_AspNetRoles 
   CONSTRAINT     ^   ALTER TABLE ONLY public."AspNetRoles"
    ADD CONSTRAINT "PK_AspNetRoles" PRIMARY KEY ("Id");
 H   ALTER TABLE ONLY public."AspNetRoles" DROP CONSTRAINT "PK_AspNetRoles";
       public         postgres    false    196            �
           2606    22365 $   AspNetUserClaims PK_AspNetUserClaims 
   CONSTRAINT     h   ALTER TABLE ONLY public."AspNetUserClaims"
    ADD CONSTRAINT "PK_AspNetUserClaims" PRIMARY KEY ("Id");
 R   ALTER TABLE ONLY public."AspNetUserClaims" DROP CONSTRAINT "PK_AspNetUserClaims";
       public         postgres    false    203            �
           2606    22378 $   AspNetUserLogins PK_AspNetUserLogins 
   CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserLogins"
    ADD CONSTRAINT "PK_AspNetUserLogins" PRIMARY KEY ("LoginProvider", "ProviderKey");
 R   ALTER TABLE ONLY public."AspNetUserLogins" DROP CONSTRAINT "PK_AspNetUserLogins";
       public         postgres    false    204    204            �
           2606    22391 "   AspNetUserRoles PK_AspNetUserRoles 
   CONSTRAINT     t   ALTER TABLE ONLY public."AspNetUserRoles"
    ADD CONSTRAINT "PK_AspNetUserRoles" PRIMARY KEY ("UserId", "RoleId");
 P   ALTER TABLE ONLY public."AspNetUserRoles" DROP CONSTRAINT "PK_AspNetUserRoles";
       public         postgres    false    205    205            �
           2606    22409 $   AspNetUserTokens PK_AspNetUserTokens 
   CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserTokens"
    ADD CONSTRAINT "PK_AspNetUserTokens" PRIMARY KEY ("UserId", "LoginProvider", "Name");
 R   ALTER TABLE ONLY public."AspNetUserTokens" DROP CONSTRAINT "PK_AspNetUserTokens";
       public         postgres    false    206    206    206            �
           2606    22330    AspNetUsers PK_AspNetUsers 
   CONSTRAINT     ^   ALTER TABLE ONLY public."AspNetUsers"
    ADD CONSTRAINT "PK_AspNetUsers" PRIMARY KEY ("Id");
 H   ALTER TABLE ONLY public."AspNetUsers" DROP CONSTRAINT "PK_AspNetUsers";
       public         postgres    false    197            �
           2606    22340    BadLinks PK_BadLinks 
   CONSTRAINT     X   ALTER TABLE ONLY public."BadLinks"
    ADD CONSTRAINT "PK_BadLinks" PRIMARY KEY ("Id");
 B   ALTER TABLE ONLY public."BadLinks" DROP CONSTRAINT "PK_BadLinks";
       public         postgres    false    199            �
           2606    22439    Links PK_Links 
   CONSTRAINT     R   ALTER TABLE ONLY public."Links"
    ADD CONSTRAINT "PK_Links" PRIMARY KEY ("Id");
 <   ALTER TABLE ONLY public."Links" DROP CONSTRAINT "PK_Links";
       public         postgres    false    210            �
           2606    22424    Projects PK_Projects 
   CONSTRAINT     X   ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "PK_Projects" PRIMARY KEY ("Id");
 B   ALTER TABLE ONLY public."Projects" DROP CONSTRAINT "PK_Projects";
       public         postgres    false    208            �
           1259    22450 
   EmailIndex    INDEX     S   CREATE INDEX "EmailIndex" ON public."AspNetUsers" USING btree ("NormalizedEmail");
     DROP INDEX public."EmailIndex";
       public         postgres    false    197            �
           1259    22445    IX_AspNetRoleClaims_RoleId    INDEX     _   CREATE INDEX "IX_AspNetRoleClaims_RoleId" ON public."AspNetRoleClaims" USING btree ("RoleId");
 0   DROP INDEX public."IX_AspNetRoleClaims_RoleId";
       public         postgres    false    201            �
           1259    22447    IX_AspNetUserClaims_UserId    INDEX     _   CREATE INDEX "IX_AspNetUserClaims_UserId" ON public."AspNetUserClaims" USING btree ("UserId");
 0   DROP INDEX public."IX_AspNetUserClaims_UserId";
       public         postgres    false    203            �
           1259    22448    IX_AspNetUserLogins_UserId    INDEX     _   CREATE INDEX "IX_AspNetUserLogins_UserId" ON public."AspNetUserLogins" USING btree ("UserId");
 0   DROP INDEX public."IX_AspNetUserLogins_UserId";
       public         postgres    false    204            �
           1259    22449    IX_AspNetUserRoles_RoleId    INDEX     ]   CREATE INDEX "IX_AspNetUserRoles_RoleId" ON public."AspNetUserRoles" USING btree ("RoleId");
 /   DROP INDEX public."IX_AspNetUserRoles_RoleId";
       public         postgres    false    205            �
           1259    22452    IX_Links_ProjectId    INDEX     O   CREATE INDEX "IX_Links_ProjectId" ON public."Links" USING btree ("ProjectId");
 (   DROP INDEX public."IX_Links_ProjectId";
       public         postgres    false    210            �
           1259    22453    IX_Projects_UserId    INDEX     O   CREATE INDEX "IX_Projects_UserId" ON public."Projects" USING btree ("UserId");
 (   DROP INDEX public."IX_Projects_UserId";
       public         postgres    false    208            �
           1259    22446    RoleNameIndex    INDEX     \   CREATE UNIQUE INDEX "RoleNameIndex" ON public."AspNetRoles" USING btree ("NormalizedName");
 #   DROP INDEX public."RoleNameIndex";
       public         postgres    false    196            �
           1259    22451    UserNameIndex    INDEX     `   CREATE UNIQUE INDEX "UserNameIndex" ON public."AspNetUsers" USING btree ("NormalizedUserName");
 #   DROP INDEX public."UserNameIndex";
       public         postgres    false    197            �
           2606    22351 7   AspNetRoleClaims FK_AspNetRoleClaims_AspNetRoles_RoleId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetRoleClaims"
    ADD CONSTRAINT "FK_AspNetRoleClaims_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES public."AspNetRoles"("Id") ON DELETE CASCADE;
 e   ALTER TABLE ONLY public."AspNetRoleClaims" DROP CONSTRAINT "FK_AspNetRoleClaims_AspNetRoles_RoleId";
       public       postgres    false    2740    196    201            �
           2606    22366 7   AspNetUserClaims FK_AspNetUserClaims_AspNetUsers_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserClaims"
    ADD CONSTRAINT "FK_AspNetUserClaims_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES public."AspNetUsers"("Id") ON DELETE CASCADE;
 e   ALTER TABLE ONLY public."AspNetUserClaims" DROP CONSTRAINT "FK_AspNetUserClaims_AspNetUsers_UserId";
       public       postgres    false    2744    203    197            �
           2606    22379 7   AspNetUserLogins FK_AspNetUserLogins_AspNetUsers_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserLogins"
    ADD CONSTRAINT "FK_AspNetUserLogins_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES public."AspNetUsers"("Id") ON DELETE CASCADE;
 e   ALTER TABLE ONLY public."AspNetUserLogins" DROP CONSTRAINT "FK_AspNetUserLogins_AspNetUsers_UserId";
       public       postgres    false    197    204    2744            �
           2606    22392 5   AspNetUserRoles FK_AspNetUserRoles_AspNetRoles_RoleId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserRoles"
    ADD CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId" FOREIGN KEY ("RoleId") REFERENCES public."AspNetRoles"("Id") ON DELETE CASCADE;
 c   ALTER TABLE ONLY public."AspNetUserRoles" DROP CONSTRAINT "FK_AspNetUserRoles_AspNetRoles_RoleId";
       public       postgres    false    2740    205    196            �
           2606    22397 5   AspNetUserRoles FK_AspNetUserRoles_AspNetUsers_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserRoles"
    ADD CONSTRAINT "FK_AspNetUserRoles_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES public."AspNetUsers"("Id") ON DELETE CASCADE;
 c   ALTER TABLE ONLY public."AspNetUserRoles" DROP CONSTRAINT "FK_AspNetUserRoles_AspNetUsers_UserId";
       public       postgres    false    2744    205    197            �
           2606    22410 7   AspNetUserTokens FK_AspNetUserTokens_AspNetUsers_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY public."AspNetUserTokens"
    ADD CONSTRAINT "FK_AspNetUserTokens_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES public."AspNetUsers"("Id") ON DELETE CASCADE;
 e   ALTER TABLE ONLY public."AspNetUserTokens" DROP CONSTRAINT "FK_AspNetUserTokens_AspNetUsers_UserId";
       public       postgres    false    206    2744    197            �
           2606    22440 !   Links FK_Links_Projects_ProjectId    FK CONSTRAINT     �   ALTER TABLE ONLY public."Links"
    ADD CONSTRAINT "FK_Links_Projects_ProjectId" FOREIGN KEY ("ProjectId") REFERENCES public."Projects"("Id") ON DELETE CASCADE;
 O   ALTER TABLE ONLY public."Links" DROP CONSTRAINT "FK_Links_Projects_ProjectId";
       public       postgres    false    2764    208    210            �
           2606    22425 '   Projects FK_Projects_AspNetUsers_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "FK_Projects_AspNetUsers_UserId" FOREIGN KEY ("UserId") REFERENCES public."AspNetUsers"("Id") ON DELETE RESTRICT;
 U   ALTER TABLE ONLY public."Projects" DROP CONSTRAINT "FK_Projects_AspNetUsers_UserId";
       public       postgres    false    208    2744    197            V      x������ � �      Q      x������ � �      X   �   x�����  k�?`��.C�K� #��(��;���u�X�&��H��0J�"��RB+����s�o ���XZų�^悽�g��҃tP)�f�_�#�6��Gfwj��5��CI"�-ゼ�A�J�!����N��O�\8�?��B�      Y      x������ � �      Z      x������ � �      [      x������ � �      R   ~  x�u�Ks�0���W侥d@/ت�`��0`W.I�^��q���K�k2�3Ӈ>��y�󕫱�;���P1��g��eAJ�w(�o��~Ȣ��I�p�����z�k{��������{�~���W��7o������W���Ull}�aC��!-��Q)%o��$����,Zm�l�2YK�������k1��ʂ��ЮS@�&��Ͼ�4�� . ~~��ox+&Ly ���T[���
f�������U�f�﹣�7C�/q�fJ�h���|�'�3��%�����J��-����y]���TH1L��e��.�4�֢nK�I:��"]�i��iLx�D��p��#����Bcalo<���Kn{���ܞ�N��K�q��\��      T      x������ � �      _   N  x�u��k�0���_!�Y�b;uc+�����(��XJ���v頰�����l�ڦMa����i�ut�Fz�}?�G�J1�	��?S�hS�م����=�7��ΰ����pmg�/����qX�k�u��B�1��R�.ˌ�az+/?QC:2��M.4�/�-.�AA��}�f^��28�b����΁���`{�kn��Y}b��Cp��p��PRQʃ�|����ob���[52��OA���~e/��¿�{
Kg��Ҋ��
qf��N����\�:G�s}j�5Y�7F�_���S��\ҟ�xZ��5��O;sM���!����@���o�����Me��í&�\��Q5�HO��Ԙ�L�$Sy"��DIUi��}�MFZT��4�hg?�ü�U�����N:�gb��M!:�U���UC񪔊����÷����� A
:W��J��HI���Z����m#������L7T�� uaZ��tC:�i�F:�O�4�[~��.�����j[F� ĭ�˂n�ȏ:� �!p�,U�e���FQ0��|0͋TL��%b�;��?8$��      ]   �   x��O;n�@�gOa)�D�?�KI�A���.� Q'4)RZ�� !R��
�7b�D�I�����F�t�X��K�|ၿyH��V�4�m�H�A�
��h�ij/ƺ�h���L�C�Z
٭Ҡ�>�?�p�m��m�O�1e͞O��ő��6���?M���������?y�K���ݤ6]���7�6�󲥀�VM�(W��`B�޹X�9H1{B\r�~�     
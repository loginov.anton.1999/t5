﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Web.Converter;
using Web.Interfaces;
using Web.Models;

namespace Web.Areas.WebApp.Controllers
{
    [Authorize]
    //[Authorize(Roles = "admin, user")]
    public class LinkController : Controller
    {
        /// <summary>
        /// current user id
        /// </summary>
        string UserId
        {
            get
            {
                return userManager.GetUserId(User);
            }
        }

        private readonly UserManager<User> userManager;
        private readonly ILinkService linkService;
        private readonly IProjectService projectService;
        private readonly IBadLinkService badLinkService;

        public LinkController(UserManager<User> userManager, ILinkService linkService, IProjectService projectService, IBadLinkService badLinkService)
        {
            this.userManager = userManager;
            this.linkService = linkService;
            this.projectService = projectService;
            this.badLinkService = badLinkService;
        }

        [HttpGet]
        public async Task<ActionResult> Links(int projectId)
        {
            Project project = await projectService.GetProjectAsync(projectId);
            project.Links = await projectService.GetLinksAsync(projectId);
            return View(project);
        }

        //public async Task<ActionResult> AllLinks()
        //{
        //    return View(await linkService.GetAvailableLinksAsync());
        //}

        public async Task<ActionResult> UserLinks()
        {
            return View(await linkService.GetUserLinksAsync(UserId));
        }

        [HttpGet]
        public async Task<ActionResult> Search(string? Name, string? DateBegin, string? DateEnd, string SortType)
        {
            List<Link> links = await linkService.GetUserLinksAsync(UserId);
            if (!string.IsNullOrEmpty(Name))
                links = links.Where(l => l.Name.Contains(Name)).ToList();
            if (!string.IsNullOrEmpty(DateBegin))
            {
                DateTime date = DateTime.Parse(DateBegin);
                links = links.Where(l => l.DateCreate > date).ToList();
            }
            if (!string.IsNullOrEmpty(DateEnd))
            {
                DateTime date = DateTime.Parse(DateEnd);
                links = links.Where(l => l.DateCreate < date).ToList();
            }
            if (!string.IsNullOrEmpty(SortType))
            {

                switch (SortType)
                {
                    case "Name":
                        links = links.OrderBy(l => l.Name).ToList();
                        break;
                    case "CountClick":
                        links = links.OrderBy(l => l.CountClick).ToList();
                        break;
                    case "DateCreate":
                        links = links.OrderBy(l => l.DateCreate).ToList();
                        break;
                }
            }
            return View("LinkSearch", links);
        }

        /// <summary>
        /// Redirects to site 
        /// </summary>
        /// <response code="200">Redirect</response>
        /// <response code="200">Link mot found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        [Route("i{shortId}")]
        [HttpGet]
        public async Task<IActionResult> Follow(string shortId)
        {
            int linkId = ShortStringConverter.StringToInt(shortId);

            Link link = await linkService.GetLinkAsync(linkId);
            if (link == null)
                return NotFound();
            if (link.IsActive)
            {
                link.CountClick++;
                await linkService.UpdateAsync(link);
                return Redirect(link.Url);
            }
            else
                return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            Link link = await linkService.GetLinkAsync(id);
            if (link != null && await projectService.CheckUser(UserId, link.ProjectId))
                return PartialView(link);
            return new NotFoundResult();
        }

        [HttpGet]
        public IActionResult Create(int projectId)
        {
            return PartialView("Edit", new Link() { ProjectId = projectId });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Link link)
        {
            if (ModelState.IsValid)
            {
                Project project = await projectService.GetProjectAsync(link.ProjectId);
                if(project==null)
                    ModelState.AddModelError("ProjectId", "Такого проекта нет");
                else
                if (!UserId.Equals(project.UserId))
                    ModelState.AddModelError("ProjectId", "Нет доступа");
                else
                if (!await badLinkService.CheckLink(link.Url))
                    ModelState.AddModelError("Url", "Данная ссылка запрещена");
                else
                if (link.Id != 0)
                    await linkService.UpdateAsync(link);
                else
                {
                    link.DateCreate = DateTime.Now;
                    await linkService.CreateAsync(link);
                }
            }
            return PartialView(link);
        }

        /// <summary>
        /// project links
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> LinkProject(int projectId)
        {
            Project project = await projectService.GetProjectAsync(projectId);
            if (project != null && await projectService.CheckUser(UserId, projectId))
            {
                project.Links = await projectService.GetLinksAsync(projectId);
                return PartialView(project);
            }
            else
            {
                ModelState.AddModelError("", "нет доступа");
                return new NotFoundResult();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            Link link = await linkService.GetLinkAsync(id);
            if (IsAdmin() || await projectService.CheckUser(UserId, link.ProjectId))
            {
                return PartialView(link);
            }
            else
            {
                ModelState.AddModelError("", "нет доступа");
                return PartialView(new Link());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Link link)
        {
            if (IsAdmin() || await linkService.CheckUser(UserId, link.Id))
                await linkService.DeleteAsync(link.Id);
            else
                ModelState.AddModelError("", "нет доступа");
            return PartialView(link);
        }

        [HttpGet]
        public async Task<IActionResult> LinksJSON(int? ProjectId)
        {
            if (ProjectId is null)
            {
                return Json(await linkService.GetUserLinksAsync(UserId));
            }
            else
            {
                int id = ProjectId ?? default(int);

                if (!await projectService.CheckUser(UserId, id))
                    return new NotFoundResult();

                return Json(await projectService.GetLinksAsync(id));
            }
        }

        //  checks that the current user is the admin
        private bool IsAdmin()
        {
            bool a = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Admin";
            return a;
            //// Get the roles for the user
            //var user = await userManager.FindByNameAsync(User.Identity.Name);
            //var roles = await userManager.GetRolesAsync(user);
            //return await userManager.IsInRoleAsync(user, "Admin");
            ////return roles.Contains("Admin");
        }
    }
}

﻿using Authorization.FacebookDemo.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Models;
namespace Web.Areas.WebApp.Controllers
{
    [Authorize]
    //[Authorize(Roles = "admin, user")]
    public class ProjectController : Controller
    {
        /// <summary>
        /// current user id
        /// </summary>
        string UserId
        {
            get
            {
                return userManager.GetUserId(User);
            }
        }

        readonly IProjectService projectService;
        readonly UserManager<User> userManager;

        public ProjectController(IProjectService projectService, UserManager<User> userManager)
        {
            //Databaseinitializer.Init(userManager);
            this.projectService = projectService;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await projectService.GetProjectsAsync(UserId));
        }

        [HttpGet]
        public async Task<IActionResult> Projects()
        {
            return View(await projectService.GetProjectsAsync(UserId));
        }

        [HttpGet]
        public async Task<IActionResult> ProjectsJSON()
        {
            return Json(await projectService.GetProjectsAsync(UserId));
        }

        [HttpGet]
        public IActionResult Create()
        {
            string userId = userManager.GetUserId(User);
            return PartialView("Edit", new Project() { UserId = userId });
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            Project project = await projectService.GetProjectAsync(id);
            if (project != null && project.UserId == UserId)
                return PartialView(project);
            else
                return new NotFoundResult();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Project project)
        {
            if (ModelState.IsValid)
            {
                if (project.Id == 0)
                {
                    project.UserId = userManager.GetUserId(User);
                    project.DateCreate = DateTime.Now;
                    await projectService.CreateAsync(project);
                }
                else
                if (!await projectService.CheckUser(UserId, project.Id))
                    ModelState.AddModelError("", "У вас нет доступа!");
                else
                    await projectService.UpdateAsync(project);
            }
            return PartialView(project);
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ViewDeleteAsync(int id)
        {
            Project project = new Project();
            if (!await projectService.CheckUser(UserId, id))
                ModelState.AddModelError("", "У вас нет доступа!");
            else
                project = await projectService.GetProjectAsync(id);
            return PartialView(project);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Project project)
        {
            if (!await projectService.CheckUser(UserId, project.Id))
                ModelState.AddModelError("", "У вас нет доступа!");
            else
                await projectService.DeleteAsync(project.Id);
            return PartialView(project);
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Models;

namespace Web.Areas.WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BlackListController : Controller
    {
        IBadLinkService badLinkService;

        public BlackListController(IBadLinkService badLinkService)
        {
            this.badLinkService = badLinkService;
        }

        [HttpGet]
        public async Task<IActionResult> BadLinks()
        {
            return View(await badLinkService.GetBadLinksAsync());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView("Edit");
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            return PartialView("Edit", badLinkService.GetBadLinkAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BadLink link)
        {
            if (ModelState.IsValid)
            {
                //uniqueness check
                if (!await badLinkService.CheckLink(link.Url))
                {
                    ModelState.AddModelError("Url", "Такая ссылка уже есть");
                    return PartialView(link);
                }
                if (link.Id == 0)
                {
                    await badLinkService.CreateAsync(link);
                }
                else
                {
                    await badLinkService.UpdateAsync(link);
                }
            }
            return PartialView(link);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            return PartialView(await badLinkService.GetBadLinkAsync(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await badLinkService.DeleteAsync(id);
            return PartialView(new BadLink() { Id = id });
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await badLinkService.GetBadLinksAsync());
        }
    }
}

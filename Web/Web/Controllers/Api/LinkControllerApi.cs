﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Web.Converter;
using Web.Interfaces;
using Web.Models;

namespace Web.Areas.Api.Controllers
{
    /// <summary>
    /// Working with Links
    /// </summary>
    /// <response code="401">Not authorized</response>
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("[controller]")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class LinkControllerApi : Controller
    {
        /// <summary>
        /// current user id
        /// </summary>
        string UserId
        {
            get
            {
                return User.Claims.Where(u => u.Type == "Id").Select(x => x.Value).SingleOrDefault();
            }
        }

        private readonly ILinkService linkService;
        private readonly IProjectService projectService;
        private readonly IBadLinkService badLinkService;

        public LinkControllerApi(ILinkService linkService, IProjectService projectService, IBadLinkService badLinkService)
        {
            this.linkService = linkService;
            this.projectService = projectService;
            this.badLinkService = badLinkService;
        }

        /// <summary>
        /// Get all the links for the project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>Link list</returns>
        /// <response code="200">Links sent</response>
        /// <response code="403">You do not have access to this project</response>
        /// <response code="404">Project not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("project/{projectId}")]
        public async Task<ActionResult<IEnumerable<Link>>> ProjectLinks(int projectId)
        {
            Project project = await projectService.GetProjectAsync(projectId);

            if (project == null)
                return NotFound();

            if (!project.UserId.Equals(UserId))
                return Forbid();

            return new OkObjectResult(await projectService.GetLinksAsync(projectId));
        }

        /// <summary>
        /// Get all your links 
        /// </summary>
        /// <returns>Link of your list</returns>
        /// <response code="200">Links sent</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Link>>> UserLinks()
        {
            return new OkObjectResult(await linkService.GetUserLinksAsync(UserId));
        }

        /// <summary>
        /// Get link by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Return link</response>
        /// <response code="403">You do not have access to this link</response>
        /// <response code="404">Your link was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<Link>> Link(int id)
        {
            Link link = await linkService.GetLinkAsync(id);

            if (link == null)
                return NotFound();

            if (!await projectService.CheckUser(UserId, link.ProjectId))
                return Forbid();

            return new OkObjectResult(link);
        }

        /// <summary>
        /// Searching and sorting your links
        /// </summary>
        /// <param name="name">search by name (contains,case insensitive)</param>
        /// <param name="dateBegin">format yyyy-mm-dd</param>
        /// <param name="dateEnd">format yyyy-mm-dd</param>
        /// <param name="sortType">Type of sort(Name/CountClick/DateCreate)</param>
        /// <returns></returns>
        /// <response code="200">Found successfully</response>
        /// <response code="400">Incorrect data</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("search")]
        public async Task<ActionResult> Search(string name, string dateBegin, string dateEnd, string sortType)
        {
            List<Link> links = await linkService.GetUserLinksAsync(UserId);
            if (!string.IsNullOrEmpty(name))
                links = links.Where(l => l.Name.Contains(name)).ToList();
            DateTime date;
            if (!string.IsNullOrEmpty(dateBegin))
            {
                if(DateTime.TryParse(dateBegin,out date))
                {
                    ModelState.AddModelError("dateBegin", "не верный формат даты");
                    return BadRequest(ModelState);
                }
                links = links.Where(l => l.DateCreate > date).ToList();
            }
            if (!string.IsNullOrEmpty(dateEnd))
            {
                if (DateTime.TryParse(dateEnd, out date))
                {
                    ModelState.AddModelError("dateEnd", "не верный формат даты");
                    return BadRequest(ModelState);
                }
                links = links.Where(l => l.DateCreate < date).ToList();
            }
            if (!string.IsNullOrEmpty(sortType))
            {

                switch (sortType)
                {
                    case "Name":
                        links = links.OrderBy(l => l.Name).ToList();
                        break;
                    case "CountClick":
                        links = links.OrderBy(l => l.CountClick).ToList();
                        break;
                    case "DateCreate":
                        links = links.OrderBy(l => l.DateCreate).ToList();
                        break;
                }
            }
            return new OkObjectResult(links);
        }

        /// <summary>Create link</summary>
        /// <param name="link"></param>
        /// <remarks>
        /// Sample request:
        /// {
        ///  "name": "string",
        ///  "description": "string",
        ///  "isActive": true,
        ///  "projectId": 4,
        ///  "url": "https://localhost:44320/swagger/index.html"
        ///}
        /// </remarks>
        /// <returns>Execution status</returns>
        /// <response code="201">New link created</response>
        /// <response code="400">Incorrect data</response>
        /// <response code="403">You do not have access to the link project</response>
        /// <response code="404">Your project was not found</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<IActionResult> CreateAsync(Link link)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await badLinkService.CheckLink(link.Url))
            {
                ModelState.AddModelError("Url", "Данная ссылка запрещена");
                return BadRequest(ModelState);
            }

            Project project = await projectService.GetProjectAsync(link.ProjectId);
            if (project == null)
                return NotFound();
            if (!project.UserId.Equals(UserId))
                return Forbid();

            link.DateCreate = DateTime.Now;
            link = await linkService.CreateAsync(link);

            return Created("", link);
        }

        /// <summary>Update link</summary>
        /// <param name="link"></param>
        /// <returns>Execution status</returns>
        /// <response code="200">Changes were successful</response>
        /// <response code="400">Incorrect data</response>
        /// <response code="403">You do not have access to this link or link project</response>
        /// <response code="404">Your link or project was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch]
        public async Task<IActionResult> Update(Link link)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await badLinkService.CheckLink(link.Url))
            {
                ModelState.AddModelError("Url", "Данная ссылка запрещена");
                return BadRequest(ModelState);
            }

            Link link2 = await linkService.GetLinkAsync(link.Id);
            if (link2 == null)
                return NotFound();

            Project project = await projectService.GetProjectAsync(link.ProjectId);
            if (project == null)
                return NotFound();

            if (!project.UserId.Equals(UserId))
                return Forbid();

            await linkService.UpdateAsync(link);

            return Ok();
        }

        /// <summary>Delete link</summary>
        /// <param name="link"></param>
        /// <returns>Execution status</returns>
        /// <response code="200">Removal was successful</response>
        /// <response code="403">You do not have access to this link</response>
        /// <response code="404">Your link was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            Link link = await linkService.GetLinkAsync(id);
            if (link == null)
                return NotFound();

            if (!(IsAdmin() || await projectService.CheckUser(UserId, link.ProjectId)))
                return Forbid();

            await linkService.DeleteAsync(id);
            return Ok();
        }

        /// <summary>Get short address for link by id</summary>
        /// <param name="id">short link id</param>
        /// <remarks>
        /// 
        /// </remarks>
        /// <returns>short address for link</returns>
        /// <response code="200">Successful</response>
        /// <response code="403">You do not have access to this link</response>
        /// <response code="404">Your link was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("ShortLink")]
        public async Task<IActionResult> ShortLink(int id)
        {
            Link link = await linkService.GetLinkAsync(id);

            if (link == null)
                return NotFound();

            if (!await projectService.CheckUser(UserId, link.ProjectId))
                return Forbid();

            string linkUrl = Request.Scheme + "://" + Request.Host +
                Url.Content($"~/i{ShortStringConverter.IntToString(link.Id)}");

            return Ok(linkUrl);
        }

        /// <summary>
        /// Get base url for Web site
        /// </summary>
        /// <returns>Base url for Web site</returns>
        //[HttpGet("BaseUrl")]
        //public IActionResult BaseUrl()
        //{
        //    //return Ok(Url.Action("Follow","Link",null, "https"));
        //    return Ok(Request.Scheme + "://" + Request.Host.Value);
        //}

        //  checks that the current user is the admin
        private bool IsAdmin()
        {
            //return User.Claims.Where(u => u.Type.Equals()).Select(x => x.Value).SingleOrDefault();

            bool a = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Admin";
            return a;
        }
    }
}

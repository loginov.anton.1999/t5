﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Models;

namespace Web.Areas.Api.Controllers
{
    /// <summary>
    /// Working with projects
    /// </summary>
    /// <response code="401">Not authorized</response>
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("[controller]")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    //[Authorize(Roles = "admin, user")]
    public class ProjectControllerApi : Controller
    {
        /// <summary>
        /// current user id
        /// </summary>
        string UserId
        {
            get
            {
                return User.Claims.Where(u => u.Type == "Id").Select(x => x.Value).SingleOrDefault();
            }
        }

        readonly IProjectService projectService;
        //readonly UserManager<User> userManager;

        //public ProjectController(IProjectService projectService, UserManager<User> userManager)
        public ProjectControllerApi(IProjectService projectService)
        {
            //Databaseinitializer.Init(userManager);
            this.projectService = projectService;
            //this.userManager = userManager;
        }

        /// <summary>
        /// Get all your projects
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Project>>> Projects()
        {
            return new OkObjectResult(await projectService.GetProjectsAsync(UserId));
        }

        /// <summary>
        /// Get a project by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Project</returns>
        /// <response code="200">Completed successfully</response>
        /// <response code="403">You do not have access to this project</response>
        /// <response code="404">Project not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> Project(int id)
        {
            Project project = await projectService.GetProjectAsync(id);

            if (project == null)
                return NotFound();

            if (!project.UserId.Equals(UserId))
                return Forbid();

            return new OkObjectResult(project);
        }

        /// <summary>Create a new project</summary>
        /// <param name="project"></param>
        /// <returns>Id project</returns>
        /// <remarks>
        /// Sample request:
        ///     POST /Todo
        ///     {
        ///         "name": "dom",
        ///         "description": ""
        ///     }
        /// </remarks>
        /// <response code="201">New project created</response>
        /// <response code="400">Required field is not filled</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<IActionResult> Create(Project project)
        {
            if (ModelState.IsValid)
            {
                project.UserId = UserId;
                project = await projectService.CreateAsync(project);

                return Created("", new { id = project.Id });
            }
            return BadRequest();
        }

        /// <summary>Update project</summary>
        /// <param name="project"></param>
        /// <returns>Execution status</returns>
        /// <response code="200">Changes were successful</response>
        /// <response code="400">Required field is not filled</response>
        /// <response code="403">You do not have access to this project</response>
        /// <response code="404">Your project was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch]
        public async Task<IActionResult> Update(Project project)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            Project project2 = await projectService.GetProjectAsync(project.Id);
            if (project2 == null)
                return NotFound();

            if (!project2.UserId.Equals(project.UserId) || !project2.UserId.Equals(UserId))
                return Forbid();

            await projectService.UpdateAsync(project);

            return Ok();
        }

        /// <summary>
        /// Delete project
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Execution status</returns>
        /// <response code="200">Removal was successful</response>
        /// <response code="403">You do not have access to this project</response>
        /// <response code="404">Your project was not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            Project project2 = await projectService.GetProjectAsync(id);

            if (project2 == null)
                return NotFound();

            if (!project2.UserId.Equals(UserId))
                return Forbid();

            await projectService.DeleteAsync(id);
            return Ok();
        }
    }
}

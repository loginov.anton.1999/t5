﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.WebApp.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        [HttpGet]
        public IActionResult Statistics()
        {
            return View();
        }
    }
}

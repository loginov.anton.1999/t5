﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.WebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //return RedirectToAction("Test");
            return RedirectToAction("Index", "Project");
            //return View();
        }
        public IActionResult Test()
        {
            return View();
        }
    }
}

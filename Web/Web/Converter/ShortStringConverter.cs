﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Converter
{
    public static class ShortStringConverter
    {
        private const string Base64Format = "AAAAAA==";
        private const char ZeroBase64Char = 'A';
        private const char AdditionalBase64Char = '=';

        public static string IntToString(int value)
        {
            var bytes = BitConverter.GetBytes(value);
            string result = Convert.ToBase64String(bytes);
            var last = result.IndexOf(result.LastOrDefault(r => r != ZeroBase64Char && r != AdditionalBase64Char));

            return result.Substring(0, last + 1);
        }

        public static int StringToInt(string value)
        {
            var base64String = $"{value}{Base64Format.Substring(value.Length)}";
            var bytes = Convert.FromBase64String(base64String);
            return BitConverter.ToInt32(bytes);
        }
    }
}

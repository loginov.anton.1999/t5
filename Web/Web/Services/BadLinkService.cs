﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.EF;
using Web.Interfaces;
using Web.Models;

namespace Web.Services
{
    public class BadLinkService : IBadLinkService
    {
        ApplicationContext db;
        public BadLinkService(ApplicationContext context)
        {
            db = context;
        }
        public async Task<List<BadLink>> GetBadLinksAsync()
        {
            return await db.BadLinks.ToListAsync();
        }

        public async Task<BadLink> GetBadLinkAsync(int id)
        {
            return await db.BadLinks.FirstOrDefaultAsync(b => b.Id == id);
        }

        public async Task<BadLink> CreateAsync(BadLink link)
        {
            db.BadLinks.Add(link);
            await db.SaveChangesAsync();
            return link;
        }
        public async Task UpdateAsync(BadLink link)
        {
            db.Update(link);
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            db.BadLinks.Remove(await GetBadLinkAsync(id));
            await db.SaveChangesAsync();
        }

        public async Task<bool> CheckLink(string url)
        {
            return await db.BadLinks.FirstOrDefaultAsync(b => url.StartsWith(b.Url)) == null;
        }
    }
}

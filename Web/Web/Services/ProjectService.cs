﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.EF;
using Web.Interfaces;
using Web.Models;

namespace Web.Services
{
    public class ProjectService : IProjectService
    {
        readonly ApplicationContext db;
        public ProjectService(ApplicationContext context)
        {
            db = context;
            //InitialDB.Initial(db);
        }

        public async Task<List<Link>> GetLinksAsync(int projectId)
        {
            Project project = await GetProjectAsync(projectId);
            await db.Entry(project).Collection(p => p.Links).LoadAsync();
            return project.Links.ToList();
        }

        public async Task<Project> GetProjectAsync(int id)
        {
            return await db.Projects.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Project> CreateAsync(Project project)
        {
            db.Projects.Add(project);
            await db.SaveChangesAsync();
            return project;
        }

        public async Task UpdateAsync(Project project)
        {
            Project pr = await GetProjectAsync(project.Id);
            pr.Description = project.Description;
            pr.Name = project.Name;
            //db.Entry(project).State = EntityState.Modified;
            db.Projects.Update(pr);
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            db.Projects.Remove(await GetProjectAsync(id));
            await db.SaveChangesAsync();
        }

        public async Task<List<Project>> GetProjectsAsync(string userId)
        {
            return await db.Projects.Where(p => p.UserId == userId).ToListAsync();
        }

        /// <summary>
        /// does the link belong to the user
        /// </summary>
        public async Task<bool> CheckUser(string userId, int projectId)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Id == userId);
            Project project = await db.Projects.FirstOrDefaultAsync(p => p.Id == projectId);
            if(user.Id == project.UserId)
                return true;
            else
                return false;
        }
    }
}

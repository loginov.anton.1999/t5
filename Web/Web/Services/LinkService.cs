﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.EF;
using Web.Interfaces;
using Web.Models;

namespace Web.Services
{
    public class LinkService : ILinkService
    {
        ApplicationContext db;
        IProjectService projectService;

        public LinkService(ApplicationContext context, IProjectService projectService)
        {
            this.projectService = projectService;
            db = context;
        }
        public async Task<Link> GetLinkAsync(int id)
        {
            return await db.Links.FirstOrDefaultAsync(l => l.Id == id);
        }

        public async Task<Link> CreateAsync(Link link)
        {
            Project project = await projectService.GetProjectAsync(link.ProjectId);
            project.CountLink++;
            db.Links.Add(link);
            await projectService.UpdateAsync(project);
            await db.SaveChangesAsync();
            return link;
        }

        public async Task UpdateAsync(Link link)
        {
            Link oldLink = await GetLinkAsync(link.Id);

            if (link.ProjectId != oldLink.ProjectId)
            {
                Project project = await projectService.GetProjectAsync(oldLink.ProjectId);
                project.CountLink--;
                await projectService.UpdateAsync(project);

                project = await projectService.GetProjectAsync(link.ProjectId);
                project.CountLink++;
                await projectService.UpdateAsync(project);
            }

            oldLink.IsActive = link.IsActive;
            oldLink.Name = link.Name;
            oldLink.ProjectId = link.ProjectId;
            oldLink.Url = link.Url;
            oldLink.Description = link.Description;

            db.Update(oldLink);
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Link link = await db.Links.FirstOrDefaultAsync(l => l.Id.Equals(id));
            Project project = await projectService.GetProjectAsync(link.ProjectId);
            project.CountLink--;
            await projectService.UpdateAsync(project);
            db.Links.Remove(link);
            await db.SaveChangesAsync();
        }

        public async Task<List<Link>> GetLinksAsync()
        {
            return await db.Links.ToListAsync();
        }

        //public async Task<List<Link>> GetAvailableLinksAsync()
        //{
        //    return await db.Links.Where(l => l.IsActive == true && l.AccessToAll == true).ToListAsync();
        //}

        /// <summary>
        ///does the link belong to the user
        /// </summary>
        public async Task<bool> CheckUser(string userId, int linkId)
        {
            Link link = await GetLinkAsync(linkId);
            if (await projectService.CheckUser(userId, link.ProjectId))
                return true;
            else
                return false;
        }

        public Task<List<Link>> GetUserLinksAsync(string userId)
        {
            var links = from project in db.Projects
                        from link in project.Links
                        where project.UserId == userId
                        select link;
            return links.ToListAsync();
        }
    }
}

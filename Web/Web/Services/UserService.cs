﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.EF;
using Web.Interfaces;
using Web.Models;

namespace Web.Services
{
    public class UserService : IUserService
    {
        ApplicationContext db;

        public UserService(ApplicationContext context)
        {
            db = context;
        }

        //public async Task<User> GetUserAsync(int id)
        //{
        //    return await db.Users.FirstOrDefaultAsync(u => u.Id == id);
        //}

        public async Task CreateAsync(User user)
        {
            db.Users.Add(user);
            await db.SaveChangesAsync();
        }

        public async Task UpdateAsync(User user)
        {
            db.Update(user);
            await db.SaveChangesAsync();
        }
        public async Task DeleteAsync(User user)
        {
            db.Users.Remove(user);
            await db.SaveChangesAsync();
        }

        public async Task<List<Project>> GetProjectsAsync(string userName)
        {
            User user = await GetUserAsync(userName);
            await db.Entry(user).Collection(u => u.Projects).LoadAsync();
            return user.Projects.ToList();
        }

        public async Task<User> GetUserAsync(string name)
        {
            return await db.Users.FirstOrDefaultAsync(u => u.UserName == name);
        }
    }
}

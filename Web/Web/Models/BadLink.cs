﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class BadLink
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Вставьте url")]
        [Url(ErrorMessage = "это не url")]
        public string Url { get; set; }
    }
}

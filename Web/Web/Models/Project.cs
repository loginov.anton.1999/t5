﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreate { get; set; }
        public string UserId { get; set; }
        public int CountLink { get; set;  }
        public ICollection<Link> Links { get; set; }
        public Project()
        {
            Links = new List<Link>();
            CountLink = 0;
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Web.Models
{
    public class User: IdentityUser
    {
        //public int Id { get; set; }
        ////[Required(ErrorMessage = "Не указано имя")]
        //public string Name { get; set; }
        ////[Required(ErrorMessage = "Не указан пароль")]
        //public string Password { get; set; }
        public DateTime DateRegister { get; set; }
        public ICollection<Project> Projects { get; set; }
        //public int? RoleId { get; set; }
        //public Role Role { get; set; }
        public User()
        {
            Projects = new List<Project>();
        }
    }
}

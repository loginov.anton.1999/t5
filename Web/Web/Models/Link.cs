﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class Link
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "придумайте имя")]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreate { get; set; }
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "выберите проект")]
        public int ProjectId { get; set; }
        //public bool AccessToAll { get; set; }

        [Required(ErrorMessage = "вставьте url")]
        [Url(ErrorMessage = "это не url")]
        public string Url { get; set; }
        public int CountClick { get; set; }

        //[NotMapped]
        //public string ShortLink { get => UriService.BaseUri; }
    }
}

﻿$("[rel=tooltip]").tooltip({ html: true });

//function Copy(message) {
//    navigator.clipboard.writeText(message)
//}
function Copy(message) {
    try {
        if (navigator.clipboard != undefined) {
            navigator.clipboard.writeText(message);
        } else if (window.clipboardData) { // для Internet Explorer
            window.clipboardData.setData('text', message);
        }
        //else { // для других браузеров, iOS, Mac OS
        //    this.copyToClipboard(this.inputEl.nativeElement);
        //}
        //this.tooltipText = 'Copied to Clipboard.'; // копирование проведено успешно.
    } catch (e) {
        //this.tooltipText = 'Please copy coupon manually.'; // копирование не удалось.
    }
}
//function copyToClipboard(el) {
//    const oldContentEditable = el.contentEditable;
//    const oldReadOnly = el.readOnly;
//    try {
//        el.contentEditable = 'true'; //  специально для iOS
//        el.readOnly = false;
//        this.copyNodeContentsToClipboard(el);
//    } finally {
//        el.contentEditable = oldContentEditable;
//        el.readOnly = oldReadOnly;
//    }
//}
//function copyNodeContentsToClipboard(el) {
//    const range = document.createRange();
//    const selection = window.getSelection();
//    range.selectNodeContents(el);
//    selection.removeAllRanges();
//    selection.addRange(range);
//    el.setSelectionRange(0, 999999);
//}


$(function () {
    var placeholderElement = $('#modal-placeholder');

    function ajaxModal() {
        $('.ajax-modal').click(function (event) {
            var url = $(this).data('url');
            $.get(url).done(function (data) {
                placeholderElement.html(data);
                placeholderElement.find('.modal').modal('show');
            });
        });
    }
    ajaxModal();

    placeholderElement.on('click', '[data-save="modal"]', function (event) {
        event.preventDefault();

        var del = ($(this).attr('modal-delete')) == 'true';

        var form = $(this).parents('.modal').find('form');
        var actionUrl = form.attr('action');
        var dataToSend = form.serialize();

        $.post(actionUrl, dataToSend).done(function (data) {
            var newBody = $('.modal-body', data);
            placeholderElement.find('.modal-body').replaceWith(newBody);

            var id = newBody.find('[name="Id"]').val();

            var isValid = newBody.find('[name="IsValid"]').val() == 'True';
            if (isValid) {
                if (del) {
                    $('[itemdelete="' + id + '"]').remove();
                }
                else {
                    var panelElement = $('#replacepanel');
                    var tableUrl = panelElement.data('url');
                    $.get(tableUrl).done(function (panel) {
                        var $data = $(panel);
                        panelElement.replaceWith($data);
                        $data.on('click', '.ajax-modal', function (event) {
                            var url = $(this).data('url');
                            $.get(url).done(function (data) {
                                placeholderElement.html(data);
                                placeholderElement.find('.modal').modal('show');
                            });
                        });
                    });
                }
                placeholderElement.find('.modal').modal('hide');
            }
        });
    });

});
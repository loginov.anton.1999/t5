﻿using System;
using System.Linq;
using Web.Models;

namespace Web.EF
{
    public class InitialDB
    {
        public static void Initial(ApplicationContext db)
        {
            User user = db.Users.FirstOrDefault(u => u.UserName == "admin");
            if (user == null) return;
            if (user.Projects.Count != 0) return;
            Project project1 = new Project()
            {
                CountLink = 2,
                Name = "Bootstrap",
                Description = "помощь по Bootstrap",
                DateCreate = DateTime.Parse("2020-04-07")
            };
            Project project2 = new Project()
            {
                CountLink = 2,
                Name = "Избранное",
                Description = "то что мне нравится",
                DateCreate = DateTime.Parse("2020-02-03")
            };
            Project project3 = new Project()
            {
                CountLink = 2,
                Name = "C#",
                Description = "помощь по C#",
                DateCreate = DateTime.Parse("2020-03-05")
            };

            Link link1 = new Link()
            {
                Name = "Flexbox · Bootstrap",
                Description = "описание примеров Flexbox в Bootstrap",
                DateCreate = DateTime.Parse("2020-05-01"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 50,
                Url = "https://v4-alpha.getbootstrap.com/utilities/flexbox/#align-self"
            };
            Link link2 = new Link()
            {
                Name = "Bootstrap 4 example",
                Description = "онлайн компилятор Bootstrap",
                DateCreate = DateTime.Parse("2020-05-02"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 70,
                Url = "https://www.codeply.com/p?starter=Bootstrap"
            };
            Link link3 = new Link()
            {
                Name = "Youtube",
                Description = "видосики",
                DateCreate = DateTime.Parse("2020-02-03"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 50,
                Url = "https://www.youtube.com"
            };
            Link link4 = new Link()
            {
                Name = "Цветовfz гамма",
                Description = "Выбор цветовой гаммы мобильного приложения | студия WOXAPP |",
                DateCreate = DateTime.Parse("2020-05-06"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 50,
                Url = "https://woxapp.com/ru/our-blog/how-the-choice-of-colors-affects-users/"
            };
            Link link5 = new Link()
            {
                Name = "C# Naming Conventions",
                Description = "",
                DateCreate = DateTime.Parse("2020-04-01"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 78,
                Url = "https://www.c-sharpcorner.com/UploadFile/8a67c0/C-Sharp-coding-standards-and-naming-conventions/"
            };
            Link link6 = new Link()
            {
                Name = "Контроллеры asp net",
                Description = "",
                DateCreate = DateTime.Parse("2020-05-14"),
                IsActive = true,
                //AccessToAll = true,
                CountClick = 50,
                Url = "https://metanit.com/sharp/mvc5/3.1.php"
            };
            project1.Links.Add(link1);
            project1.Links.Add(link2);
            project2.Links.Add(link3);
            project2.Links.Add(link4);
            project3.Links.Add(link5);
            project3.Links.Add(link6);
            user.Projects.Add(project1);
            user.Projects.Add(project2);
            user.Projects.Add(project3);
            db.Update(user);
            db.SaveChanges();
        }
    }
}

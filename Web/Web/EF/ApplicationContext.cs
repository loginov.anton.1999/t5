﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Web.Models;

namespace Web.EF
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<BadLink> BadLinks { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
            //Role adminRole = new Role() { Id = 1, Name = "admin" };
            //Role userRole = new Role() { Id = 2, Name = "user" };
            //User user = new User() { Id = 1, RoleId = 2, Name = "a", Password = "a", DateRegister = DateTime.Parse("2020-01-01") };
            //User admin = new User() { Id = 2, RoleId = 1, Name = "admin", Password = "admin", DateRegister = DateTime.Parse("2020-01-01") };
            //BadLink link = new BadLink() { Id = 1, Url = "https://vk.com" };
            //Project project1 = new Project()
            //{
            //    Id = 1,
            //    UserId = 1,
            //    CountLink = 2,
            //    Name = "Bootstrap",
            //    Description = "помощь по Bootstrap",
            //    DateCreate = DateTime.Parse("2020-04-07")
            //};
            //Project project2 = new Project()
            //{
            //    Id = 2,
            //    UserId = 1,
            //    CountLink = 2,
            //    Name = "Избранное",
            //    Description = "то что мне нравится",
            //    DateCreate = DateTime.Parse("2020-02-03")
            //};
            //Project project3 = new Project()
            //{
            //    Id = 3,
            //    UserId = 1,
            //    CountLink = 2,
            //    Name = "C#",
            //    Description = "помощь по C#",
            //    DateCreate = DateTime.Parse("2020-03-05")
            //};

            //Link link1 = new Link()
            //{
            //    Id = 1,
            //    ProjectId = 1,
            //    Name = "Flexbox · Bootstrap",
            //    Description = "описание примеров Flexbox в Bootstrap",
            //    DateCreate = DateTime.Parse("2020-05-01"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 50,
            //    Url = "https://v4-alpha.getbootstrap.com/utilities/flexbox/#align-self"
            //};
            //Link link2 = new Link()
            //{
            //    Id = 2,
            //    ProjectId = 1,
            //    Name = "Bootstrap 4 example",
            //    Description = "онлайн компилятор Bootstrap",
            //    DateCreate = DateTime.Parse("2020-05-02"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 70,
            //    Url = "https://www.codeply.com/p?starter=Bootstrap"
            //};
            //Link link3 = new Link()
            //{
            //    Id = 3,
            //    ProjectId = 2,
            //    Name = "Youtube",
            //    Description = "видосики",
            //    DateCreate = DateTime.Parse("2020-02-03"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 50,
            //    Url = "https://www.youtube.com"
            //};
            //Link link4 = new Link()
            //{
            //    Id = 4,
            //    ProjectId = 2,
            //    Name = "Цветовfz гамма",
            //    Description = "Выбор цветовой гаммы мобильного приложения | студия WOXAPP |",
            //    DateCreate = DateTime.Parse("2020-05-06"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 50,
            //    Url = "https://woxapp.com/ru/our-blog/how-the-choice-of-colors-affects-users/"
            //};
            //Link link5 = new Link()
            //{
            //    Id = 5,
            //    ProjectId = 3,
            //    Name = "C# Naming Conventions",
            //    Description = "",
            //    DateCreate = DateTime.Parse("2020-04-01"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 78,
            //    Url = "https://www.c-sharpcorner.com/UploadFile/8a67c0/C-Sharp-coding-standards-and-naming-conventions/"
            //};
            //Link link6 = new Link()
            //{
            //    Id = 6,
            //    ProjectId = 3,
            //    Name = "Контроллеры asp net",
            //    Description = "",
            //    DateCreate = DateTime.Parse("2020-05-14"),
            //    IsActive = true,
            //    AccessToAll = true,
            //    CountClick = 50,
            //    Url = "https://metanit.com/sharp/mvc5/3.1.php"
            //};
            //modelBuilder.Entity<Role>().HasData(userRole);
            //modelBuilder.Entity<Role>().HasData(adminRole);
            //modelBuilder.Entity<User>().HasData(user);
            //modelBuilder.Entity<User>().HasData(admin);
            //modelBuilder.Entity<Project>().HasData(project1);
            //modelBuilder.Entity<Project>().HasData(project2);
            //modelBuilder.Entity<Project>().HasData(project3);

            //modelBuilder.Entity<Link>().HasData(link1);
            //modelBuilder.Entity<Link>().HasData(link2);
            //modelBuilder.Entity<Link>().HasData(link3);
            //modelBuilder.Entity<Link>().HasData(link4);
            //modelBuilder.Entity<Link>().HasData(link5);
            //modelBuilder.Entity<Link>().HasData(link6);

            //modelBuilder.Entity<BadLink>().HasData(link);

        //    base.OnModelCreating(modelBuilder);
        //}
    }
}

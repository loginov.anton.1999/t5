﻿using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Api.ViewModels
{
    public class UserViewModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

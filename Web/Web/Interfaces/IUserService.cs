﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Interfaces
{
    public interface IUserService
    {
        //Task<User> GetUserAsync(int id);
        Task<User> GetUserAsync(string name);
        Task CreateAsync(User user);
        Task UpdateAsync(User user);
        Task DeleteAsync(User user);
        Task<List<Project>> GetProjectsAsync(string userName);
    }
}

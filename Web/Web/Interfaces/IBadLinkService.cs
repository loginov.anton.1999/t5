﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Interfaces
{
    public interface IBadLinkService
    {
        Task<List<BadLink>> GetBadLinksAsync();
        Task<BadLink> GetBadLinkAsync(int id);
        Task<BadLink> CreateAsync(BadLink link);
        Task UpdateAsync(BadLink link);
        Task DeleteAsync(int id);

        /// <summary>
        /// True if the link is not found
        /// </summary>
        Task<bool> CheckLink(string url);
    }
}

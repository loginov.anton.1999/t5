﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Interfaces
{
    public interface ILinkService
    {
        Task<List<Link>> GetLinksAsync();
        //Task<List<Link>> GetAvailableLinksAsync();
        Task<List<Link>> GetUserLinksAsync(string userId);
        Task<Link> GetLinkAsync(int id);
        Task<Link> CreateAsync(Link link);
        Task UpdateAsync(Link link);
        Task DeleteAsync(int id);
        Task<bool> CheckUser(string userId, int linkId);
    }
}

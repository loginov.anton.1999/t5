﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Interfaces
{
    public interface IProjectService
    {
        Task<List<Link>> GetLinksAsync(int projectId);
        Task<List<Project>> GetProjectsAsync(string userId);
        Task<Project> GetProjectAsync(int id);
        Task<Project> CreateAsync(Project project);
        Task UpdateAsync(Project project);
        Task DeleteAsync(int id);
        Task<bool> CheckUser(string userId, int projectId);
    }
}

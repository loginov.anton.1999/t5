﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Web.Areas.Api.Controllers;
using Web.Interfaces;
using Web.Models;
using WebApi.Controllers;
using WebApi.Tests.Data;
using Xunit;

namespace WebApi.Tests.Tests
{
    public class ProjectControllerTests
    {
        private readonly ProjectController projectController;
        List<Project> projects = new DataTests().Projects;
        public ProjectControllerTests()
        {
            var mockProjectService = new Mock<IProjectService>();

            mockProjectService.Setup(s => s.GetProjectsAsync(It.IsAny<string>()))
                .ReturnsAsync((string userId) => projects.Where(p => p.UserId.Equals(userId)).ToList());

            mockProjectService.Setup(s => s.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync((int id) => projects.FirstOrDefault(p => p.Id.Equals(id)));

            mockProjectService.Setup(s => s.CreateAsync(It.IsAny<Project>()))
                .ReturnsAsync((Project project) =>
                {
                    projects.Add(project);
                    return project;
                });

            mockProjectService.Setup(s => s.UpdateAsync(It.IsAny<Project>()))
                .Callback((Project project) =>
                {
                    Project projectOld = projects.FirstOrDefault(p => p.Id.Equals(project.Id));
                    projectOld.Name = project.Name;
                    projectOld.DateCreate = project.DateCreate;
                    projectOld.Description = project.Description;
                });

            mockProjectService.Setup(s => s.DeleteAsync(It.IsAny<int>()))
                .Callback((int id) =>
                {
                    Project project = projects.FirstOrDefault(b => b.Id.Equals(id));
                    projects.Remove(project);
                });

            projectController = new ProjectController(mockProjectService.Object);

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
{
                //new Claim(ClaimsIdentity.DefaultNameClaimType, ""),
                //new Claim(ClaimsIdentity.DefaultRoleClaimType, "User"),
                new Claim("Id",DataTests.UserId)
            });

            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(claimsIdentity));
            projectController.ControllerContext.HttpContext = contextMock.Object;
        }

        [Fact]
        public async void GetProjectsReturnsListOfProjects()
        {
            // Act
            var result = (await projectController.Projects()).Result as OkObjectResult;

            // Assert
            Assert.Equal(projects.Where(p=>p.UserId.Equals(DataTests.UserId)).Count(), (result.Value as List<Project>).Count);
        }

        [Fact]
        public async void GetProjectReturnsRequestedProject()
        {
            // Arrange
            int id = 1;

            // Act
            var result = (await projectController.Project(id)).Result as OkObjectResult;

            // Assert
            Assert.Equal(projects.FirstOrDefault(b => b.Id.Equals(id)), result.Value as Project);
        }

        [Fact]
        public async void GetProjectForbiddenReturnsForbidResult()
        {
            // Arrange
            int id = 2;

            // Act
            var result = (await projectController.Project(id)).Result;

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void GetNonExistentProjectReturnsNotFoundResult()
        {
            // Arrange
            int id = 100;

            // Act
            var result = (await projectController.Project(id)).Result;

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void CreateSuccessfulReturnsCreatedResult()
        {
            // Arrange
            Project project = new Project()
            {
                Id = 10,
                Name= "opi",
                DateCreate = DateTime.Now,
                UserId = DataTests.UserId
            };

            // Act
            var result = await projectController.Create(project);

            // Assert
            Assert.IsType<CreatedResult>(result);
            Assert.Contains(project, projects);
        }

        [Fact]
        public async void UpdateSuccessfulReturnsOkResult()
        {
            // Arrange
            Project project = new Project()
            {
                Id = 1,
                UserId = DataTests.UserId,
                Name = "oneNew",
                Description = "imNew",
                DateCreate = DateTime.Now
            };

            // Act
            var result = await projectController.Update(project);

            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void UpdateNotAvailableProjectReturnsForbidResult()
        {
            // Arrange
            Project project = new Project()
            {
                Id = 2,
                UserId = DataTests.UserId,
                Name = "oneNew",
                Description = "imNew",
                DateCreate = DateTime.Now
            };

            // Act
            var result = await projectController.Update(project);

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void UpdateNonExistentProjectReturnsNotFoundResult()
        {
            // Arrange
            Project project = new Project()
            {
                Id = 111,
                UserId = DataTests.UserId,
                Name = "oneNew",
                Description = "imNew",
                DateCreate = DateTime.Now
            };

            // Act
            var result = await projectController.Update(project);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void DeleteSuccessfulReturnsOkResult()
        {
            // Arrange
            int id = 1;
            Project project = projects.FirstOrDefault(p => p.Id.Equals(id));

            // Act
            var result = await projectController.Delete(id);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.DoesNotContain(project, projects);
        }

        [Fact]
        public async void DeleteNotAvailableProjectReturnsForbidResult()
        {
            // Arrange
            int id = 2;

            // Act
            var result = await projectController.Delete(id);

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void DeleteNonExistentProjectReturnsNotFoundResult()
        {
            // Arrange
            int id = 1111;

            // Act
            var result = await projectController.Delete(id);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Web.Areas.Api.Controllers;
using Web.Interfaces;
using Web.Models;
using WebApi.Controllers;
using WebApi.Tests.Data;
using Xunit;

namespace WebApi.Tests.Tests
{
    public class BlackListControllerTests
    {
        private readonly BlackListController blackListController;
        private readonly List<BadLink> badLinks;
        public BlackListControllerTests()
        {
            badLinks = new DataTests().BadLinks;
            var mockRepo = new Mock<IBadLinkService>();

            mockRepo.Setup(repo => repo.GetBadLinksAsync()).ReturnsAsync(() => badLinks.ToList());
            mockRepo.Setup(repo => repo.GetBadLinkAsync(It.IsAny<int>()))
                .ReturnsAsync((int id) => badLinks.FirstOrDefault(b => b.Id.Equals(id)));

            mockRepo.Setup(repo => repo.CheckLink(It.IsAny<string>()))
                .ReturnsAsync((string url) => badLinks.FirstOrDefault(b => url.StartsWith(b.Url)) == null);

            mockRepo.Setup(repo => repo.CreateAsync(It.IsAny<BadLink>()))
                .ReturnsAsync((BadLink badLink) =>
                {
                    badLinks.Add(badLink);
                    return badLink;
                });

            mockRepo.Setup(repo => repo.UpdateAsync(It.IsAny<BadLink>()))
                .Callback((BadLink badLink) =>
                {
                    BadLink badLinkOld = badLinks.FirstOrDefault(b => b.Id.Equals(badLink.Id));
                    badLinkOld.Url = badLink.Url;
                });

            mockRepo.Setup(repo => repo.DeleteAsync(It.IsAny<int>()))
                .Callback((int id) =>
                {
                    BadLink badLink = badLinks.FirstOrDefault(b=>b.Id.Equals(id));
                    badLinks.Remove(badLink);
                });

            blackListController = new BlackListController(mockRepo.Object);
        }

        [Fact]
        public async void GetBadLinksReturnsListOfBadLinks()
        {
            // Act
            var result = (await blackListController.BadLinks()).Result as OkObjectResult;

            // Assert
            Assert.Equal(badLinks.Count, (result.Value as List<BadLink>).Count);
        }

        [Fact]
        public async void GetBadLinkByIdReturnsRequestedBadLink()
        {
            // Arrange
            int id = 111;

            // Act
            var result = (await blackListController.BadLink(id)).Result as OkObjectResult;

            // Assert
            Assert.Equal(badLinks.FirstOrDefault(b => b.Id.Equals(id)), result.Value as BadLink);
        }

        [Fact]
        public async void GetBadLinkByInvalidIdReturnsNotFoundResult()
        {
            // Arrange  
            int id = 119;


            // Act
            var result = (await blackListController.BadLink(id)).Result;

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void CreateSuccessfulReturnsCreatedResult()
        {
            // Arrange
            BadLink badLink = new BadLink()
            {
                Id = 2177,
                Url = "https://assss.com"
            };

            // Act
            var result = (await blackListController.CreateAsync(badLink)).Result;

            // Assert
            Assert.IsType<CreatedResult>(result);
            Assert.Contains(badLink, badLinks);
        }

        //[Fact]
        //public async void CreationWithSimilarUrlReturnsBadRequestResult()
        //{
        //    // Arrange
        //    BadLink badLink = new BadLink()
        //    {
        //        Id = 2177,
        //        Url = "https://vk.com"
        //    };

        //    // Act
        //    var result = (await blackListController.CreateAsync(badLink)).Result;

        //    // Assert
        //    Assert.IsType<BadRequestObjectResult>(result);
        //}

        [Fact]
        public async void UpdateSuccessfulReturnsOkResult()
        {
            // Arrange
            BadLink badLink = new BadLink()
            {
                Id = 111,
                Url = "https://mmmmm"
            };

            // Act
            var result = await blackListController.Update(badLink);

            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void DeleteSuccessfulReturnsOkResult()
        {
            // Arrange
            int id = 111;
            BadLink badLink = badLinks.FirstOrDefault(b =>b.Id.Equals(id));

            // Act
            var result = await blackListController.Delete(id);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.DoesNotContain(badLink, badLinks);
        }

        [Fact]
        public async void DeleteInvalidIdReturnsNotFoundResult()
        {
            // Arrange
            int id = 311;

            // Act
            var result = await blackListController.Delete(id);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}

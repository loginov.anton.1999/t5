﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Web.Interfaces;
using Web.Models;
using WebApi.Controllers;
using WebApi.Tests.Data;
using Xunit;

namespace WebApi.Tests.Tests
{
    public class LinkControllerTests
    {
        private readonly LinkController linkController;
        List<Link> links;
        List<Project> projects;
        List<BadLink> badLinks;
        public LinkControllerTests()
        {
            DataTests dataTests = new DataTests();
            links = new DataTests().Links;
            projects = new DataTests().Projects;
            badLinks = new DataTests().BadLinks;

            var mockProjectService = new Mock<IProjectService>();
            var mockLinkService = new Mock<ILinkService>();
            var mockBadLinkService = new Mock<IBadLinkService>();

            #region mockLinkService
            mockLinkService.Setup(s => s.GetUserLinksAsync(It.IsAny<string>()))
                .ReturnsAsync((string userId) =>
                    (from project in projects
                     from link in project.Links
                     where project.UserId == userId
                     select link).ToList());

            mockLinkService.Setup(s => s.GetLinkAsync(It.IsAny<int>()))
                .ReturnsAsync((int id) => links.FirstOrDefault(p => p.Id.Equals(id)));

            mockLinkService.Setup(s => s.CreateAsync(It.IsAny<Link>()))
                .ReturnsAsync((Link link) =>
                {
                    projects.FirstOrDefault(p => p.Id.Equals(link.ProjectId)).Links.Add(link);
                    links.Add(link);
                    return link;
                });

            mockLinkService.Setup(s => s.UpdateAsync(It.IsAny<Link>()))
                .Callback((Link link) =>
                {
                    Link linkOld = links.FirstOrDefault(p => p.Id.Equals(link.Id));
                    linkOld.Name = link.Name;
                    linkOld.Description = link.Description;
                    linkOld.IsActive = link.IsActive;
                    linkOld.ProjectId = link.ProjectId;
                    linkOld.Url = link.Url;
                });

            mockLinkService.Setup(s => s.DeleteAsync(It.IsAny<int>()))
                .Callback((int id) =>
                {
                    Link link = links.FirstOrDefault(b => b.Id.Equals(id));
                    links.Remove(link);
                });
            #endregion

            #region mockProjectService
            mockProjectService.Setup(s => s.GetProjectAsync(It.IsAny<int>()))
                .ReturnsAsync((int id) => projects.FirstOrDefault(p => p.Id.Equals(id)));

            mockProjectService.Setup(s => s.CheckUser(It.IsAny<string>(), It.IsAny<int>()))
                .ReturnsAsync((string userId, int projectId) => projects.FirstOrDefault(p => p.Id.Equals(projectId)).UserId.Equals(userId));

            mockProjectService.Setup(s => s.GetLinksAsync(It.IsAny<int>()))
                .ReturnsAsync((int projectId) => projects.FirstOrDefault(p => p.Id.Equals(projectId)).Links.ToList());
            #endregion

            #region mockBadLinkService
            mockBadLinkService.Setup(repo => repo.CheckLink(It.IsAny<string>()))
                .ReturnsAsync((string url) => badLinks.FirstOrDefault(b => url.StartsWith(b.Url)) == null);
            #endregion

            linkController = new LinkController(mockLinkService.Object, mockProjectService.Object, mockBadLinkService.Object);

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Role, "User"),
                new Claim("Id",DataTests.UserId)
            });

            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(claimsIdentity));
            linkController.ControllerContext.HttpContext = contextMock.Object;
        }

        [Fact]
        public async void GetUserLinksReturnsListOfUserLinks()
        {
            // Arrange
            var checkCountLinks = (from project in projects
                                   from link in project.Links
                                   where project.UserId == DataTests.UserId
                                   select link).Count();

            // Act
            var result = (await linkController.UserLinks()).Result as OkObjectResult;

            // Assert
            Assert.Equal(checkCountLinks, (result.Value as List<Link>).Count);
        }

        [Fact]
        public async void GetLinkByIdReturnsRequestedLink()
        {
            // Arrange
            int id = 1;

            // Act
            var result = (await linkController.Link(id)).Result as OkObjectResult;

            // Assert
            Assert.Equal(links.FirstOrDefault(b => b.Id.Equals(id)), result.Value as Link);
        }

        [Fact]
        public async void GetLinkForbiddenReturnsForbidResult()
        {
            // Arrange
            int id = 4;

            // Act
            var result = (await linkController.Link(id)).Result;

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void GetNonExistentLinkReturnsNotFoundResult()
        {
            // Arrange
            int id = 100;

            // Act
            var result = (await linkController.Link(id)).Result;

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void GetLinksByProjectIdReturnsRequestedLink()
        {
            // Arrange
            int projectId = 4;
            var linksCountCheck = projects.FirstOrDefault(p => p.Id.Equals(projectId)).Links.Count();

            // Act
            var result = (await linkController.ProjectLinks(projectId)).Result as OkObjectResult;

            // Assert
            Assert.Equal(linksCountCheck, (result.Value as List<Link>).Count);
        }

        [Fact]
        public async void GetProjectLinksForbiddenReturnsForbidResult()
        {
            // Arrange
            int projectId = 2;

            // Act
            var result = (await linkController.ProjectLinks(projectId)).Result;

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void GetNonExistentProjectLinksReturnsNotFoundResult()
        {
            // Arrange
            int projectId = 400;

            // Act
            var result = (await linkController.ProjectLinks(projectId)).Result;

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void CreateSuccessfulReturnsCreatedResult()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 12,
                Name = "Pin",
                Url = "https://www.pinterest.com",
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 1
            };

            // Act
            var result = await linkController.CreateAsync(link);

            // Assert
            Assert.IsType<CreatedResult>(result);
            Assert.Contains(link, links);
        }

        [Fact]
        public async void CreateBadLinkReturnsBadRequest()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 12,
                Name = "Pin",
                Url = "https://vk.com",//bad link
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 1
            };

            // Act
            var result = await linkController.CreateAsync(link);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void CreateLinkForUnavailableProjectReturnsForbidden()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 12,
                Name = "Pin",
                Url = "https://vvvvvk.com",
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 2//UnavailableProject
            };

            // Act
            var result = await linkController.CreateAsync(link);

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void CreateLinkForNotExistingProjectReturnsNotFound()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 12,
                Name = "Pin",
                Url = "https://vvvvvk.com",
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 20//Not Existing Project
            };

            // Act
            var result = await linkController.CreateAsync(link);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void UpdateSuccessfulReturnsOkResult()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 1,
                Name = "Pin",
                Url = "https://vvvvvk.com",
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 4
            };

            // Act
            var result = await linkController.Update(link);

            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async void UpdateBadLinkReturnsBadRequest()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 1,
                Name = "Pin",
                Url = "https://vk.com",//bad link
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 20
            };

            // Act
            var result = await linkController.Update(link);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void UpdateLinkForUnavailableProjectReturnsForbidden()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 1,
                Name = "Pin",
                Url = "https://vfffk.com",//bad link
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 2//UnavailableProject
            };

            // Act
            var result = await linkController.Update(link);

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void UpdateForNotExistingProjectReturnsNotFound()
        {
            // Arrange
            Link link = new Link()
            {
                Id = 1,
                Name = "Pin",
                Url = "https://vfffk.com",//bad link
                Description = "asad",
                CountClick = 5,
                DateCreate = DateTime.Now,
                IsActive = true,
                ProjectId = 20
            };

            // Act
            var result = await linkController.Update(link);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void DeleteSuccessfulReturnsOkResult()
        {
            // Arrange
            int id = 1;
            Link link = links.FirstOrDefault(p => p.Id.Equals(id));

            // Act
            var result = await linkController.Delete(id);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.DoesNotContain(link, links);
        }

        [Fact]
        public async void DeleteNotAvailableLinkReturnsForbidResult()
        {
            // Arrange
            int id = 4;//NotAvailableLink

            // Act
            var result = await linkController.Delete(id);

            // Assert
            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async void DeleteNonExistentProjectReturnsNotFoundResult()
        {
            // Arrange
            int id = 1111;

            // Act
            var result = await linkController.Delete(id);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}

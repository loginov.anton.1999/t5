﻿using System;
using System.Collections.Generic;
using Web.Models;

namespace WebApi.Tests.Data
{
    public class DataTests
    {
        public static string UserId = "11112222333344445555";
        public List<Link> Links;
        public List<Project> Projects;
        public List<BadLink> BadLinks;
        public DataTests()
        {
            BadLinks = new List<BadLink>
            {
                new BadLink() { Id = 111, Url = "https://metanit.com/sharp" },
                new BadLink() { Id = 114, Url = "https://vk.com" },
                new BadLink() { Id = 117, Url = "https://www.youtube.com" }
            };

            Projects = new List<Project>()
            {
                new Project()
                {
                    Id = 1,
                    UserId = UserId,
                    Name = "one",
                    Description = "im", DateCreate = DateTime.Now
                },
                new Project()
                {
                    Id = 2,
                    UserId = "222222222222",
                    Name = "one2",
                    Description = "im", DateCreate = DateTime.Now,
                    Links = new List<Link>()
                    {
                        new Link()
                        {
                            Id = 4,
                            Name = "Pin",
                            Url = "https://www.pinterest.com",
                            Description = "asad",
                            CountClick = 5,
                            DateCreate = DateTime.Now,
                            IsActive = true,
                            ProjectId = 2
                        },
                    }
                },
                new Project()
                {
                    Id = 3,
                    UserId = UserId,
                    Name = "one3",
                    Description = "im", DateCreate = DateTime.Now
                },
                new Project()
                {
                    Id = 4,
                    UserId = UserId,
                    Name = "one4",
                    Description = "im", DateCreate = DateTime.Now,
                    Links = new List<Link>()
                    {
                        new Link()
                        {
                            Id = 2,
                            Name = "Tel",
                            Url = "https://web.telegram.org",
                            Description = "sss",
                            CountClick = 51,
                            DateCreate = DateTime.Now,
                            IsActive = true,
                            ProjectId = 4
                        },
                        new Link()
                        {
                            Id = 3,
                            Name = "aut",
                            Url = "https://author.today/",
                            Description = "sss",
                            CountClick = 51,
                            DateCreate = DateTime.Now,
                            IsActive = true,
                            ProjectId = 4
                        },
                        new Link()
                        {
                            Id = 1,
                            Name = "goo",
                            Url = "https://www.google.com/",
                            Description = "sss",
                            CountClick = 51,
                            DateCreate = DateTime.Now,
                            IsActive = true,
                            ProjectId = 4
                        }
                    }
                }
            };
            Links = new List<Link>();
            Projects.ForEach(p => Links.AddRange(p.Links));
        }
    }
}

﻿using Web.Converter;
using Xunit;

namespace Web.UnitTests
{
    public class ShortStringConverterTest
    {
        [Theory]
        [InlineData(1, "AQ")]
        [InlineData(113213353, "qX+/Bg")]
        [InlineData(113, "cQ")]
        public void IntToString_WhenNormalValue_ThenConvertToString(int value, string expected)
        {
            // Act
            var actual = ShortStringConverter.IntToString(value);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("AQ", 1)]
        [InlineData("qX+/Bg", 113213353)]
        [InlineData("cQ", 113)]
        public void StringToInt_WhenNormalValue_ThenConvertToInt(string value, int expected)
        {
            // Act
            var actual = ShortStringConverter.StringToInt(value);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}

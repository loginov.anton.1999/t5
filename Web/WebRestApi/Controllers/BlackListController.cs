﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Models;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "Admin")]
    [ApiController]
    [Route("api/[controller]")]
    public class BlackListController : Controller
    {
        IBadLinkService badLinkService;

        public BlackListController(IBadLinkService badLinkService)
        {
            this.badLinkService = badLinkService;
        }

        /// <summary>
        /// Get a bad link by id
        /// </summary>
        /// <returns>requested link</returns>
        /// <response code="200">Returns the requested link</response>
        /// <response code="404">Link not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BadLink>> BadLink(int id)
        {
            BadLink badLink = await badLinkService.GetBadLinkAsync(id);

            if (badLink == null)
                return NotFound();
            
            return new OkObjectResult(badLink);
        }

        /// <summary>
        /// Get all bad links
        /// </summary>
        /// <returns>all bad links</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BadLink>>> BadLinks()
        {
            //return await badLinkService.GetBadLinksAsync();
            return new OkObjectResult(await badLinkService.GetBadLinksAsync());
        }

        /// <summary>
        /// Create a new bad link
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        /// <response code="201">Link created</response>
        /// <response code="400">Invalid link</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<BadLink>> CreateAsync(BadLink link)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //uniqueness check
            //if (!await badLinkService.CheckLink(link.Url))
            //{
            //    ModelState.AddModelError("Url", "Такая ссылка уже есть");
            //    return BadRequest(ModelState);
            //}

            link = await badLinkService.CreateAsync(link);
         
            return Created("", new { id = link.Id });
        }

        /// <summary>
        /// Update bad link
        /// </summary>
        /// <param name="link"></param>
        /// <returns>Status code</returns>
        /// <response code="200">Changes saved</response>
        /// <response code="400">This link already exists</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPatch]
        public async Task<IActionResult> Update(BadLink link)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //uniqueness check
            //if (!await badLinkService.CheckLink(link.Url))
            //{
            //    ModelState.AddModelError("Url", "Такая ссылка уже есть");
            //    return BadRequest(ModelState);
            //}

            await badLinkService.UpdateAsync(link);

            return Ok();
        }

        /// <summary>
        /// Remove bad link
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Link successfully deleted</response>
        /// <response code="404">Bad link not found</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            BadLink badLink = await badLinkService.GetBadLinkAsync(id);
            if (badLink == null)
                return NotFound();

            await badLinkService.DeleteAsync(id);
            return Ok();
        }
    }
}
